package com.wsr.dec.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.wsr.dec.R

// Set of Material typography styles to start with
val Typography = Typography(
    /* Other default text styles to override
    titleLarge = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 22.sp,
        lineHeight = 28.sp,
        letterSpacing = 0.sp
    ),
    labelSmall = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Medium,
        fontSize = 11.sp,
        lineHeight = 16.sp,
        letterSpacing = 0.5.sp
    )
    */
)

object Typeface {
    object Sizes {
        val extraLarge = 40.sp
        val large = 32.sp
        val medium = 24.sp
        val small = 14.sp
    }
    val fontFamily = font

    object Styles {
        val extraLarge = TextStyle(
            fontWeight = FontWeight.Bold,
            fontFamily = fontFamily,
            fontSize = Sizes.extraLarge
        )
        val large = TextStyle(
            fontWeight = FontWeight.Bold,
            fontFamily = fontFamily,
            fontSize = Sizes.large
        )
        val medium = TextStyle(
            fontWeight = FontWeight.Normal,
            fontFamily = fontFamily,
            fontSize = Sizes.medium
        )
        val small = TextStyle(
            fontWeight = FontWeight.Normal,
            fontFamily = fontFamily,
            fontSize = Sizes.small
        )
    }
}

private val font = FontFamily(
    Font(R.font.font_regular, weight = FontWeight.Normal),
    Font(R.font.font_bold, weight = FontWeight.Bold)
)
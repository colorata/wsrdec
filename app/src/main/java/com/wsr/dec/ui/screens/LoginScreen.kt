package com.wsr.dec.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.wsr.dec.R
import com.wsr.dec.Strings
import com.wsr.dec.ui.components.WsrButton
import com.wsr.dec.ui.components.WsrTextField
import com.wsr.dec.ui.navigation.LocalNavController
import com.wsr.dec.ui.theme.Typeface
import com.wsr.dec.ui.theme.WsrDecTheme
import com.wsr.dec.viewmodel.LoginViewModel
import kotlinx.coroutines.launch

@Composable
fun LoginScreen(modifier: Modifier = Modifier) {
    val vm = viewModel<LoginViewModel>()
    val scope = rememberCoroutineScope()
    val context = LocalContext.current
    val navController = LocalNavController.current

    val secondaryColor = MaterialTheme.colorScheme.secondary
    Column(
        Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.surface)
            .padding(horizontal = 24.dp)
    ) {
        Image(
            painter = painterResource(id = R.drawable.login_image),
            contentDescription = null,
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(1f)
        )
        Spacer(modifier = Modifier.size(24.dp))
        Text(
            text = Strings.signIn,
            style = Typeface.Styles.large,
            color = MaterialTheme.colorScheme.onSurface
        )
        Spacer(modifier = Modifier.size(18.dp))
        WsrTextField(value = vm.login, onValueChange = {
            vm.login = it
        }, label = Strings.login, modifier = Modifier.fillMaxWidth())
        Spacer(modifier = Modifier.size(12.dp))
        WsrTextField(value = vm.password, onValueChange = {
            vm.password = it
        }, label = Strings.password, modifier = Modifier.fillMaxWidth(), showSymbols = false) {
            Icon(
                painter = painterResource(id = R.drawable.ic_secure),
                contentDescription = null,
                modifier = Modifier.size(24.dp)
            )
        }
        Spacer(modifier = Modifier.size(24.dp))
        WsrButton(
            label = Strings.login,
            onClick = {
                scope.launch {
                    vm.login(context, navController)
                }
            },
            icon = painterResource(id = R.drawable.arrow_forward),
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.size(24.dp))
        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(buildAnnotatedString {
                withStyle(
                    SpanStyle(
                        color = secondaryColor
                    )
                ) {
                    append(Strings.forgotPassword + " ")
                }
                withStyle(
                    SpanStyle(
                        fontWeight = FontWeight.Bold
                    )
                ) {
                    append(Strings.getNew)
                }
            }, style = Typeface.Styles.small, color = MaterialTheme.colorScheme.onSurface)
            Spacer(modifier = Modifier.size(8.dp))
            Text(buildAnnotatedString {
                withStyle(
                    SpanStyle(
                        color = secondaryColor
                    )
                ) {
                    append(Strings.doYouHaveAnAccount + " ")
                }
                withStyle(
                    SpanStyle(
                        fontWeight = FontWeight.Bold
                    )
                ) {
                    append(Strings.createNew)
                }
            }, style = Typeface.Styles.small, color = MaterialTheme.colorScheme.onSurface)
        }
    }
    if (vm.errorDialogMessage != null) {
        AlertDialog(onDismissRequest = { vm.errorDialogMessage = null }, confirmButton = {
            TextButton(onClick = { vm.errorDialogMessage = null }) {
                Text(text = Strings.ok)
            }
        }, title = {
            Text(vm.errorDialogMessage ?: "")
        })
    }
}

@Preview
@Composable
private fun LoginScreenPreview() {
    WsrDecTheme {
        LoginScreen()
    }
}
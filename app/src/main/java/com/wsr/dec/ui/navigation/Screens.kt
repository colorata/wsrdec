package com.wsr.dec.ui.navigation

enum class Screens {
    Splash,
    Login,
    Tasks
}
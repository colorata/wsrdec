package com.wsr.dec.ui.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.wsr.dec.ui.theme.Typeface
import com.wsr.dec.ui.theme.WsrDecTheme

@Composable
fun WsrButton(
    label: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    icon: Painter? = null
) {
    Button(
        onClick = { onClick() },
        shape = MaterialTheme.shapes.extraSmall,
        modifier = modifier
    ) {
        Text(
            text = label,
            style = Typeface.Styles.medium,
            modifier = Modifier.padding(12.dp)
        )

        if (icon != null) {
            Icon(
                painter = icon,
                contentDescription = null,
                modifier = Modifier.size(22.dp)
            )
        }
    }
}

@Preview
@Composable
private fun WsrButtonPreview() {
    WsrDecTheme {
        WsrButton(
            label = "Login",
            onClick = { /*TODO*/ },
            modifier = Modifier.padding(24.dp)
        )
    }
}
package com.wsr.dec.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import com.wsr.dec.Strings
import com.wsr.dec.data.NetApi
import com.wsr.dec.ui.theme.Typeface
import com.wsr.dec.viewmodel.TasksViewModel

@Composable
fun TasksScreen(modifier: Modifier = Modifier) {
    val vm = viewModel<TasksViewModel>()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {
        vm.updateUserData(context)
    }
    Column(
        Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.surface)
            .padding(24.dp)
    ) {
        Row(
            Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = Strings.myTasks,
                style = Typeface.Styles.extraLarge,
                color = MaterialTheme.colorScheme.onSurface
            )
            if (vm.user != null) {
                AsyncImage(
                    model = (NetApi.storageUrl + "/" + vm.user?.data?.avatar),
                    contentDescription = ""
                )
            }
        }
    }
}
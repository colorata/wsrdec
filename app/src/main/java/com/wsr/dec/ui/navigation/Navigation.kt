package com.wsr.dec.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.compositionLocalOf
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.wsr.dec.ui.screens.LoginScreen
import com.wsr.dec.ui.screens.SplashScreen
import com.wsr.dec.ui.screens.TasksScreen
import java.lang.IllegalStateException

@Composable
fun Navigation() {
    CompositionLocalProvider(LocalNavController provides rememberNavController()) {
        NavHost(LocalNavController.current, Screens.Splash.name) {
            composable(Screens.Splash.name) {
                SplashScreen()
            }
            composable(Screens.Login.name) {
                LoginScreen()
            }
            composable(Screens.Tasks.name) {
                TasksScreen()
            }
        }
    }
}

val LocalNavController =
    compositionLocalOf<NavHostController> { throw IllegalStateException("No navController") }
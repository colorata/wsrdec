package com.wsr.dec.ui.screens

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.fadeIn
import androidx.compose.animation.scaleIn
import androidx.compose.animation.slideInVertically
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.produceState
import androidx.compose.ui.Alignment
import androidx.compose.ui.BiasAlignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.TransformOrigin
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.wsr.dec.R
import com.wsr.dec.Strings
import com.wsr.dec.data.Preferences
import com.wsr.dec.ui.navigation.LocalNavController
import com.wsr.dec.ui.navigation.Screens
import com.wsr.dec.ui.theme.Typeface
import com.wsr.dec.ui.theme.WsrDecTheme
import kotlinx.coroutines.delay

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun SplashScreen(modifier: Modifier = Modifier) {
    val navController = LocalNavController.current
    val context = LocalContext.current
    val isLaunched by produceState(false) {
        value = true
        delay(1000)
        if (Preferences.token(context) == "") {
            navController.navigate(Screens.Login.name)
        } else {
            navController.navigate(Screens.Tasks.name)
        }
    }

    Box(
        modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.primary)
    ) {
        CircleImage(
            visible = isLaunched,
            imageId = R.drawable.splash1,
            enter = scaleIn(transformOrigin = TransformOrigin(0.4f, 0.4f)),
            horizontalBias = -0.3f,
            verticalBias = -0.5f,
            modifier = Modifier.size(200.dp)
        )
        CircleImage(
            visible = isLaunched,
            imageId = R.drawable.splash4,
            enter = scaleIn(transformOrigin = TransformOrigin(0.8f, 0.3f)),
            horizontalBias = 0.8f,
            verticalBias = -0.6f,
            modifier = Modifier.size(133.dp)
        )
        CircleImage(
            visible = isLaunched,
            imageId = R.drawable.splash3,
            enter = scaleIn(transformOrigin = TransformOrigin(0.3f, 0.8f)),
            horizontalBias = -1.2f,
            verticalBias = -0.1f,
            modifier = Modifier.size(174.dp)
        )
        CircleImage(
            visible = isLaunched,
            imageId = R.drawable.splash2,
            enter = scaleIn(transformOrigin = TransformOrigin(0.8f, 0.5f)),
            horizontalBias = 0.7f,
            verticalBias = -0.1f,
            modifier = Modifier.size(149.dp)
        )
        SplashScreenText(isLaunched)
    }
}

@Composable
private fun BoxScope.CircleImage(
    visible: Boolean,
    imageId: Int,
    modifier: Modifier = Modifier,
    enter: EnterTransition,
    horizontalBias: Float,
    verticalBias: Float
) {
    AnimatedVisibility(
        visible, enter = enter, modifier = modifier.align(
            BiasAlignment(
                horizontalBias, verticalBias
            )
        )
    ) {
        Image(
            painter = painterResource(id = imageId),
            contentDescription = null,
            modifier = Modifier
        )
    }
}

@Composable
fun BoxScope.SplashScreenText(visible: Boolean, modifier: Modifier = Modifier) {
    Column(modifier.align(BiasAlignment(0f, 0.5f))) {
        AnimatedVisibility(visible, enter = slideInVertically { it * 3 } + fadeIn()) {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Text(
                    text = Strings.appName,
                    style = Typeface.Styles.extraLarge,
                    color = MaterialTheme.colorScheme.onPrimary
                )
                Text(
                    text = Strings.splashScreenSubtitle,
                    style = Typeface.Styles.medium,
                    color = MaterialTheme.colorScheme.onPrimary
                )
            }
        }
    }
}

@Preview
@Composable
private fun SplashScreenPreview() {
    WsrDecTheme {
        SplashScreen()
    }
}
package com.wsr.dec

import androidx.compose.ui.text.AnnotatedString

object Strings {
    const val myTasks = "My Tasks"
    const val ok = "Ok"
    const val appName = "TrainingSkills"

    const val splashScreenSubtitle = "Prepare for competitions"

    const val signIn = "Sign In"
    const val login = "Login"
    const val password = "Password"
    const val forgotPassword = "Forgot password?"
    const val getNew = "Get new"
    const val doYouHaveAnAccount = "Do you have an account?"
    const val createNew = "Create new"

    const val loginEmpty = "Login empty"
    const val passwordEmpty = "Password empty"
    const val loginIncorrect = "Login incorrect"
    const val passwordIncorrect = "Password incorrect"
}
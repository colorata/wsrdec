package com.wsr.dec.data

import android.util.Patterns

object Checker {
    fun isEmailCorrect(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}
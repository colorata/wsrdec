package com.wsr.dec.data

import kotlinx.serialization.Serializable

@Serializable
data class UserResponse(
    val data: UserResponseData
)

@Serializable
data class UserResponseData(
    val id: Int,
    val name: String,
    val surname: String,
    val skill_id: Int,
    val skill: UserResponseDataSkill,
    val avatar: String
)

@Serializable
data class UserResponseDataSkill(
    val id: Int,
    val title: String
)
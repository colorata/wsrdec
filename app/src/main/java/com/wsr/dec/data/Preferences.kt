package com.wsr.dec.data

import android.content.Context
import android.content.SharedPreferences

object Preferences {
    fun token(context: Context): String {
        return context
            .getSharedPreferences("prefs", Context.MODE_PRIVATE)
            .getString("token", "")
            ?: ""
    }

    fun saveToken(context: Context, value: String) {
        context
            .getSharedPreferences("prefs", Context.MODE_PRIVATE)
            .edit()
            .putString("token", value)
            .apply()
    }
}
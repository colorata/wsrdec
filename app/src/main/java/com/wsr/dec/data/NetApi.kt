package com.wsr.dec.data

import io.ktor.client.HttpClient
import io.ktor.client.engine.android.Android
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.request.get
import io.ktor.client.request.headers
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.bodyAsText
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpStatusCode
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

object NetApi {
    private val client = HttpClient(Android) {
        install(ContentNegotiation)
    }

    @OptIn(ExperimentalSerializationApi::class)
    private val json = Json {
        ignoreUnknownKeys = true
        explicitNulls = false
    }

    private const val baseUrl = "https://sept2022.mad.hakta.pro/api"
    const val storageUrl = "https://sept2022.mad.hakta.pro/storage"

    suspend fun login(
        login: String,
        password: String,
        onSuccess: (LoginResponse) -> Unit,
        onFailure: () -> Unit,
    ) {
        val response = client.post("$baseUrl/login") {
            setBody(json.encodeToString(LoginRequest(login, password)))
            headers {
                append(HttpHeaders.Accept, "application/json")
                append(HttpHeaders.ContentType, "application/json")
            }
        }
        if (response.status == HttpStatusCode.OK) {
            val body = json.decodeFromString<LoginResponse>(response.bodyAsText())
            onSuccess(body)
        } else {
           onFailure()
        }
    }

    suspend fun getUser(
        token: String,
        onSuccess: (UserResponse) -> Unit,
        onFailure: () -> Unit
    ) {
        val response = client.get("$baseUrl/user") {
            headers {
                append(HttpHeaders.Authorization, token)
                append(HttpHeaders.Accept, "application/json")
                append(HttpHeaders.ContentType, "application/json")
            }
        }
        if (response.status == HttpStatusCode.OK) {
            onSuccess(json.decodeFromString(response.bodyAsText()))
        } else {
            println(token)
            println(response.bodyAsText())
            onFailure()
        }
    }
}
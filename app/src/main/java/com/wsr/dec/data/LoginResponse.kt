package com.wsr.dec.data

import kotlinx.serialization.Serializable

@Serializable
data class LoginResponse(
    val data: LoginResponseData
)

@Serializable
data class LoginResponseData(
    val token: String,
    val name: String
)
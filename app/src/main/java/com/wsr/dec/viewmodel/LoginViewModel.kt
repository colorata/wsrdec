package com.wsr.dec.viewmodel

import android.content.Context
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.navigation.NavHostController
import com.wsr.dec.Strings
import com.wsr.dec.data.Checker
import com.wsr.dec.data.NetApi
import com.wsr.dec.data.Preferences
import com.wsr.dec.ui.navigation.Screens

class LoginViewModel: ViewModel() {
    var login by mutableStateOf("")
    var password by mutableStateOf("")

    var errorDialogMessage by mutableStateOf<String?>(null)
    suspend fun login(context: Context, navController: NavHostController) {
        if (login.isEmpty()) {
            errorDialogMessage = Strings.loginEmpty
            return
        } else if (password.isEmpty()) {
            errorDialogMessage = Strings.passwordEmpty
            return
        } else if (!Checker.isEmailCorrect(login)) {
            errorDialogMessage = Strings.loginIncorrect
            return
        }
        NetApi.login(login, password, onSuccess = {
            Preferences.saveToken(context, it.data.token)
            navController.navigate(Screens.Tasks.name)
        }, onFailure = {
            errorDialogMessage = Strings.passwordIncorrect
        })
    }
}
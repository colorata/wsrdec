package com.wsr.dec.viewmodel

import android.content.Context
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.wsr.dec.data.NetApi
import com.wsr.dec.data.Preferences
import com.wsr.dec.data.UserResponse

class TasksViewModel : ViewModel() {
    var user by mutableStateOf<UserResponse?>(null)

    suspend fun updateUserData(context: Context) {
        NetApi.getUser(Preferences.token(context), onSuccess = {
            user = it
        }, onFailure = {

        })
    }
}